#data "aws_availability_zones" "all" {}
variable "rds_disk_size" {
  description = "The disk size of the rds instance"
  default     = 10
}

variable "rds_disk_type" {
  description = "The RDS disk type that will be used"
  default     = "gp2"
}

variable "db_instance_name" {
  description = "The name of the RDS instance that Terraform will use."
}

variable "is_this_multi_az" {
  description = "Whether this is a multi AZ instance of not (boolean)"
  default     = false
}

variable "db_port" {
  description = "The database port"
  default     = 5432
}

variable "db_instance_type" {
  description = "The instance type of this instance"
  default     = "db.m3.medium"
}

variable "primary_db_name" {
  description = "The name of the database that will be hosted on this instance"
}

variable "db_administrator_user" {
  description = "database administrator user"
  default     = "master"
}

variable "db_administrator_passwd" {
  description = "The administrator password"
}

variable "owner_email" {
  description = "This describes the real life user that created/requested this database"
}

resource "aws_db_instance" "ando_db" {
  allocated_storage      = "${var.rds_disk_size}"
  storage_type           = "${var.rds_disk_type}"
  skip_final_snapshot    = true
  engine                 = "postgres"
  engine_version         = "9.6.8"
  multi_az               = "${var.is_this_multi_az}"
  port                   = "${var.db_port}"
  instance_class         = "${var.db_instance_type}"
  name                   = "${var.primary_db_name}"
  username               = "${var.db_administrator_user}"
  password               = "${var.db_administrator_passwd}"
  parameter_group_name   = "${aws_db_parameter_group.standard-prod-postgres9-6-paramgroup.id}"
  publicly_accessible    = "true"
  vpc_security_group_ids = ["${aws_security_group.db_ando_sg.id}"]

  tags = {
    "Name"        = "${var.db_instance_name}"
    "Provisioner" = "Terraform"
    "Owner"       = "${var.owner_email}"
  }
}
