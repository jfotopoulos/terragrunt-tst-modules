variable "bucket_name" {
  description = "Specify the name of the S3 bucket"
}

variable "bucket2_name" {
  description = "Specify the name of the S3 bucket"
}

variable "owner_email" {
  description = "The email of the owner of this S3 bucket"
  default     = "fotopoulos@workable.com"
}

resource "aws_s3_bucket" "terragrunt_s3_bucket" {
  bucket = "${var.bucket_name}"
  acl    = "private"

  tags {
    Name    = "${var.bucket_name}"
    Provider = "Terraform"
    Owner   = "${var.owner_email}"
  }
}

resource "aws_s3_bucket_policy" "terragrunt_s3_bucket_policy" {
  bucket = "${var.bucket_name}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::156460612806:root"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::${var.bucket_name}/*/AWSLogs/674549574953/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "terragrunt_s3_bucket2" {
  bucket = "${var.bucket2_name}"
  acl    = "private"

  tags {
    Name    = "${var.bucket2_name}"
    Provider = "Terraform"
    Owner   = "${var.owner_email}"
  }
}

