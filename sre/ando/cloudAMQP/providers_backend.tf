variable "cloudamqp_apikey" {
  description = "This is the account API key."
}

provider "cloudamqp" {
  apikey = "${var.cloudamqp_apikey}"
}

terraform {
  backend "s3" {}
}
