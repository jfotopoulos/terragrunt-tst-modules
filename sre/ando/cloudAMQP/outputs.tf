output "rmq_url" {
  value = "${cloudamqp_instance.terraform_tst2.url}"
}

output "amqp_instance_name" {
  value = "${cloudamqp_instance.terraform_tst2.name}"
}
