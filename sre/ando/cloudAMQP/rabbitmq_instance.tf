variable "cloudamqp_node_name" {
  description = "This is the name that the cloudamqp node should have. This is a unique name per account."
}

variable "cloudamqp_plan" {
  description = "Check out README.md in the module directory of cloudAMQP for a list of eligible plans."
  default     = "lemur"
}

variable "cloudamqp_region" {
  description = "Check out README.md in the module directory of cloudAMQP for a list of eligible regions/providers."
  default     = "amazon-web-services::us-east-1"
}

resource "cloudamqp_instance" "terraform_tst2" {
  name   = "${var.cloudamqp_node_name}"
  plan   = "${var.cloudamqp_plan}"
  region = "${var.cloudamqp_region}"

  #vpc_subnet = "10.0.0.0/24"
}
