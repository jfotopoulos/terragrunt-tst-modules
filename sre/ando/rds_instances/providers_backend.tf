variable "aws_region" {
  default = "us-east-2"
}

variable "aws_profile" {
  description = "The AWS profile to use for this account"
}

provider "aws" {
  version = "~> 1.5"
  region  = "${var.aws_region}"
  profile = "${var.aws_profile}"
}

terraform {
  backend "s3" {}
}
