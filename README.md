## Terraform structure tst

microservice contains:
 - Create 2 ec2 instances
 - create an s3 bucket (name should contain service name, environment)
 - create an rds postgresql db - multi az
 - create a load balancer for the ec2 instances
 - create a dns record for the load balancer
 - create security groups for ec2 instances, elb, rds. only ec2 instances should have access to the rds, only elb should have access to ec2 instances.
 - create this in different environments, accounts, regions (staging1/us-east-1, staging2/eu-west-1, sandbox-stg1/us-east-2)
 - create remote state per microservice/per environment
 - add tags to all resources (stg1, stg2, sandbox-stg1)
 - try a state file for higher up levels.. for example, remove everything a team has, or, remove all apps the team has in X environment
 - check if workspaces could work for us
 
 try not to repeat anything at all!!

 microservice name: wrkbl-srvc


 team (core) =>
 	env (staging 1) =>
 		web-application1
 		database1
 		web-application2

sourcing
	ando:
	- IAM user
	- s3 bucket
	- rds
	- rabbitmq (compose)
	- elastic (elasticloud)