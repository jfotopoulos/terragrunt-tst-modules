resource "aws_security_group" "db_ando_sg" {
  name        = "db_ando_sg"
  description = "Allow all incoming traffic on postgres port"

  ingress {
    from_port   = "${var.db_port}"
    to_port     = "${var.db_port}"
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name        = "db_ando_sg"
    Provisioner = "Terraform"
  }
}
